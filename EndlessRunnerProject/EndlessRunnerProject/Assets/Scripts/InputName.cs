﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputName : MonoBehaviour
{
    [SerializeField]
    protected List<Text> texts;

    protected char[] CurrentName;

    protected int CurrentCharacter = -1;

    protected ScoreManager manager = null;

    protected int ASCIILow = 65;

    protected int ASCIIHigh = 90;

    // Start is called before the first frame update
    void Start()
    {
        CurrentName = new char[texts.Count];

        for (int i = 0; i < texts.Count; i++)
        {
            if (texts[i].text.Length > 0)
            {
                CurrentName[i] = texts[i].text[0];
            }
        }

        manager = ScoreManager.GetScoreManagerInstance();

       
    }

    //change a letter in the name
    protected void ChangeLetter(int letterindex, int increase, bool positive)
    {
        if (letterindex < CurrentName.Length)
        {
            int temp = CurrentName[letterindex];

            if (positive)
            {
                temp += increase;
            }
            else
            {
                temp -= increase;
            }

            if (temp > ASCIIHigh)
            {
                temp = ASCIILow;
            }

            if (temp < ASCIILow)
            {
                temp = ASCIIHigh;
            }

            CurrentName[letterindex] = (char)temp;

            texts[letterindex].text = CurrentName[letterindex] + "";
        }
    }

    //increase a letter
    public void AddToLetter(int letterindex)
    {
        if (letterindex < CurrentName.Length)
        {
            ChangeLetter(letterindex, 1, true);
        }
    }

    //subtract a letter
    public void SubtractToLetter(int letterindex)
    {
        if (letterindex < CurrentName.Length)
        {
            ChangeLetter(letterindex, 1, false);
        }
    }

    //get the currently inputted name
    public string GetName()
    {
        return "" + CurrentName;
    }

    //send the name to the scoremanager
    public void SendName()
    {
        if (manager)
        {
            string result = new string(CurrentName);
            manager.ChangeName(result);
            manager.ShowHighScore(true);
        }
    }
}
