﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlatformSpawnable : MonoBehaviour
{
    protected SpawnableType poolTag = SpawnableType.Coin;

    protected bool TagSet = false;

    protected static PlatformPool pool = null;
    protected static RunnerCharacterController controller = null;
    protected static LevelCreator levelCreator = null;

    // Start is called before the first frame update
    protected void Start()
    {
        if(!pool)
        {
            pool = PlatformPool.GetObjectPoolInstance();
        }

        if(!controller)
        {
            controller = RunnerCharacterController.GetRunnerInstance();
        }

        if(!levelCreator)
        {
            levelCreator = LevelCreator.GetLevelCreatorInstance();
        }

        Setup();
    }

    protected virtual void Setup()
    {

    }

    public void SpawnObject()
    {
        Spawn();
    }

    //abstract funtion to be called when object is spawned
    protected abstract void Spawn();

    //returns the object to the object pool if it exixts
    public virtual void ReturnToPool()
    {
        StopAllCoroutines();

        if(pool && TagSet)
        {
            pool.ReturnSpawnToPool(this, poolTag);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    //set the object pool tag
    public void SetPoolTag(SpawnableType type)
    {
        if(!TagSet)
        {
            TagSet = true;
            poolTag = type;
        }
    }

    //function that can be overriden by any spawnable to calculate health change
    public virtual int ChangeHealth(int health, out float invincibilityTime)
    {
        invincibilityTime = 0.0f;

        return health;
    }

    //function that can be override by spawnable to change score change
    public virtual int ChangeScore(int score)
    {
        return score;
    }

}
