﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//interface to send collision to an obstacle
public interface IObstacle : IEventSystemHandler
{
    void SendCollision(Vector3 velocity);
}

