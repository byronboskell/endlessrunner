﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnChance", menuName = "SpawnChanceTable")]
public class SpawnChanceTable : ScriptableObject
{
    //stores spawn chances for different objects in the pool
    [Serializable]
    public class SpawnChance
    {
        [Range(0.0f, 1.0f)]
        public float spawnChance = 0.5f;
        public SpawnableType type = SpawnableType.Coin;
    }

    //stores a list of spawn tables so you can change spawn rates and what spawns between levels
    [Serializable]
    public class Table
    {
        [Header("Spawn Variables")]
        [Range(0.0f, 1.0f)]
        public float spawnChance = 0.5f;
        public int maxSpawnables = 1;

        [Header("Spawn Table")]
        public List<SpawnChance> table = new List<SpawnChance>();
    }
    
    [SerializeField]
    protected List<Table> spawnTable = new List<Table>();

    protected PlatformPool pool = null;

    protected static int currentLevel = 0;

    //roll whether or not an spawnable is spawned
    public bool RollSpawnChance()
    {
        bool result = false;

        if (currentLevel > -1 && currentLevel < spawnTable.Count)
        {
            float r = UnityEngine.Random.Range(0.0f, 1.0f);

            if (r <= spawnTable[currentLevel].spawnChance)
            {
                result = true;
            }
        }

        return result;
    }

    //spawn a collectable in the given range of slots
    public PlatformSpawnable SpawnObject(Transform parent)
    {
        PlatformSpawnable spawnable = null;

        if (!pool)
        {
            pool = PlatformPool.GetObjectPoolInstance();
        }

        if (pool && parent && spawnTable.Count > 0)
        {
            //roll to see whether or not a heart is spawned instead of a coins
            float r = UnityEngine.Random.Range(0.0f, 1.0f);

            float chance = 0.0f;

            int chosenIndex = 0;

            if(currentLevel > -1 && currentLevel <spawnTable.Count)
            {
                for (int i = 0; i < spawnTable.Count; i++)
                {
                    chance += spawnTable[currentLevel].table[i].spawnChance;

                    if (chance >= r)
                    {
                        chosenIndex = i;
                        break;
                    }
                }

                spawnable = pool.GetSpawn(spawnTable[currentLevel].table[chosenIndex].type, parent);
            }
            else
            {
                Debug.Log("Current Level: " + currentLevel + " is out of range for table! ");
            }
      
        }

        return spawnable;
    }

    //increase the current level, use level creator to ensure only level creater can control level
    public static void SetLevel(LevelCreator levelCreator)
    {
        if(levelCreator)
        {
            currentLevel = levelCreator.GetLevel();
        }

    }
    
    //get the maximum number of spawnable objects
    public int GetMaxSpawnable(int maxSlots)
    {
        int result = 0;

        if(spawnTable.Count > 0)
        {
            int index = currentLevel;

            if (currentLevel > spawnTable.Count)
            {
                index = spawnTable.Count - 1;
            }

            if(currentLevel < 0)
            {
                index = 0;
            }

            result = spawnTable[index].maxSpawnables;

            if(result > maxSlots)
            {
                result = maxSlots;
            }
        }

        return result;
    }


}
