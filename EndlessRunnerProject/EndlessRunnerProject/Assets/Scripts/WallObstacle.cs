﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallObstacle : PlatformSpawnable
{
    [SerializeField]
    protected int damageAmount = 1;

    [SerializeField]
    protected float despawnTime = 1.0f;

    [SerializeField]
    protected float invincibilityOnHit = 1.0f;

    [SerializeField]
    protected float hitForce = 20;

    [SerializeField]
    protected Vector3 FloatingOffset = Vector3.one;

    [SerializeField]
    protected bool floating = false;

    protected bool respawning = false;

    protected Rigidbody rigidBody = null;

    protected Quaternion baseRotation = Quaternion.identity;

    protected float currentDespawnTime = 0.0f;

    protected override void Setup()
    {
        base.Setup();

        rigidBody = GetComponent<Rigidbody>();

        baseRotation = transform.rotation;

        gameObject.tag = "Obstacle";
    }

    protected void Update()
    {
        if(respawning)
        {
            currentDespawnTime += Time.deltaTime;

            if(currentDespawnTime > despawnTime)
            {
                respawning = false;
                ResetForRespawn();
            }
        }
    }

    protected override void Spawn()
    {
        if (rigidBody)
        {
            rigidBody.isKinematic = true;
        }

        respawning = false;

        if(floating)
        {
            transform.Translate(FloatingOffset);
        }

    }


    //reset the object's rigidbody for return to objectpool
    protected void ResetForRespawn()
    {
        respawning = false;

        if (rigidBody)
        {
            rigidBody.isKinematic = true;
        }

        transform.rotation = baseRotation;

        ReturnToPool();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !respawning)
        {
            controller.TakeDamage(this);

            if (rigidBody)
            {
                rigidBody.isKinematic = false;
                rigidBody.AddForce(other.transform.forward * hitForce);
            }

            if (!respawning)
            {
                respawning = true;

                currentDespawnTime = 0.0f;
            }

        }

    }

    //function that calculates health change
    public override int ChangeHealth(int health, out float invincibilityTime)
    {
        invincibilityTime = invincibilityOnHit;

        health -= Mathf.Abs(damageAmount);

        return health;
    }

}
