﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class LevelCreator : MonoBehaviour
{
    public enum LevelState { Normal, Bonus, HighHazard}

    [Serializable]
    public class Level
    {
        public float maxSpeed = 10.0f;
        public float acceleration = 1.0f;
        public int normalPlatforms = 7;
        public int bonusPlatforms = 2;
        public float scoreModifier = 8.0f;
    }


    [Header("Platform Spawning")]
    [SerializeField]
    protected List<Platform> startingPlatforms = new List<Platform>();

    [Tooltip("How far away the player needs to be to spawn a platform")]
    [SerializeField]
    protected float minSpawnDistance = 1.0f;

    [Tooltip("How far apart platforms spawn")]
    [SerializeField]
    protected float gapWidth = 3;

    [SerializeField]
    protected int platformsUntilReset = 6;

    [SerializeField]
    protected int queueSize = 5;

    [SerializeField]
    protected float bonusLevelGapWidth = 0;

    [Header("Level")]

    [SerializeField]
    protected List<Level> levels = new List<Level>();

    [Header("Tables")]
    [SerializeField]
    protected SpawnChanceTable hazardTable = null;

    [SerializeField]
    protected SpawnChanceTable collectableTable = null;

    [SerializeField]
    protected SpawnChanceTable bonusTable = null;

    [SerializeField]
    protected SpawnChanceTable highHazards = null;


    [Header("Platform Movement")]
    [SerializeField]
    protected float platformStartingSpeed = 5.0f;

    [SerializeField]
    protected Vector3 platformDirection = new Vector3(-1, 0, 0);

    [Header("Events")]
    [SerializeField]
    protected UnityEvent onLevelUp = null;

    protected LevelState currentState = LevelState.Normal;

    protected SpawnChanceTable currentHazardTable = null;
    protected SpawnChanceTable currentCollectableTable = null;

    //platform speed variables
    protected float platformCurrentSpeed = 1.0f;
    protected float platformMaxSpeed = 10.0f;
    protected float currentAcceleration = 0.1f;

    //platform spawning distance variables
    protected int currentPlatformCount = 0;
    protected int platformsUntilLevelUp = 5;
    protected int bonusLevelLength = 3;

    protected Platform previousTile = null;
    protected Queue<Platform> currentQueue = new Queue<Platform>();

    //other object singelton references
    protected RunnerCharacterController character = null;
    protected ScoreManager manager = null;
    protected PlatformPool pool = null;
    protected Camera mainCamera = null;

    //parents for the platforms
    protected GameObject currentparent = null;
    protected GameObject previousparent = null;

    //rotation variables
    protected Quaternion oldRotation = Quaternion.identity;
    protected Quaternion targetRotation = Quaternion.identity;
    protected bool rotatingLevel = false;
    protected float rotationDuration = 1.0f;
    protected float currentRotationTime = 0.0f;

    //current level variables
    protected int level = 0;
    protected int levelUpPlatformNumber = 0;
    protected float currentGap = 0.0f;
    protected int currentBonusPlatformCount = 0;
    protected float currentScoreMultiplier = 8.0f;

    //starting location of character for bounds checking
    protected Vector3 spawnPosition = Vector3.zero;

    //singelton instance
    protected static LevelCreator levelCreator = null;

    protected void Awake()
    {
        if(!levelCreator)
        {
            levelCreator = this;
        }
        else
        {
            Debug.Log("An instance of the level creator already exists!");
            Destroy(this);
        }


        level = 0;

        //set the current level for all spawntables
        SpawnChanceTable.SetLevel(this);
    }

    protected void Start()
    {
        pool = PlatformPool.GetObjectPoolInstance();
        character = RunnerCharacterController.GetRunnerInstance();
        manager = ScoreManager.GetScoreManagerInstance();
        mainCamera = Camera.main;

        currentparent = new GameObject("PlatformParentOne");
        previousparent = new GameObject("PlatformParentTwo");

        platformCurrentSpeed = platformStartingSpeed;

        levelUpPlatformNumber = platformsUntilLevelUp;

        if (pool)
        {
            pool.CreatePools();

            //add in starting platforms so they can correctly be moved in the world
            for(int i=0; i< startingPlatforms.Count; i++)
            {
                if(startingPlatforms[i])
                {
                    currentQueue.Enqueue(startingPlatforms[i]);
                    previousTile = startingPlatforms[i];
                    startingPlatforms[i].transform.SetParent(currentparent.transform);
                }
            }
        }

        SetLevelVariables();

        if (manager)
        {
            manager.UpdateScoreModifier();
        }

        ChangeState(LevelState.Normal);

    }

    // do late so that the player has a chance to move in update if necessary
    protected void LateUpdate()
    {
        if(!rotatingLevel)
        {
            //if the first platform nothing will happen in update
            if (character && previousTile)
            {
                currentparent.transform.position += platformDirection * platformCurrentSpeed * Time.deltaTime;

                if (platformCurrentSpeed < platformMaxSpeed)
                {
                    platformCurrentSpeed += currentAcceleration * Time.deltaTime;
                }

                float distance = Vector3.Distance(previousTile.gameObject.transform.position, character.transform.position);

                if (distance < minSpawnDistance)
                {
                    SpawnPlatform();

                    currentPlatformCount++;

                    //every so often we reset the positions of the parents so that we don't eventually run into floating point problems
                    if (currentPlatformCount >= platformsUntilReset)
                    {
                        ResetPlatformParent();
                    }

                    //if we reach the max queue size we can despawn a platform
                    if (currentQueue.Count > queueSize)
                    {
                        Platform oldestTile = currentQueue.Dequeue();

                        oldestTile.ReturnToPool();
                    }

                    if (currentPlatformCount > levelUpPlatformNumber)
                    {
                        LevelUp();
                    }


                }
            }
        }
        else
        {

            //slerp uses times between 0 & 1, we divide the timer by that amount so is in the correct range, but also keeping correct time
            currentRotationTime += Time.deltaTime / rotationDuration;

            mainCamera.transform.rotation = Quaternion.Slerp(oldRotation, targetRotation, currentRotationTime);

            //at t = 1, the rotation has reached the correct value
            if(currentRotationTime > 1)
            {
                //ensure that the rotation is exactly what we specified
                mainCamera.transform.rotation = targetRotation;
                rotatingLevel = false;
            }

        }
    }

    //rotates the level by the given amount
    public void RotateLevel(RotatePower power)
    {
        if(!rotatingLevel && power)
        {
            rotatingLevel = true;

            //set values for slerp of rotation
            oldRotation = mainCamera.transform.rotation;
            targetRotation = oldRotation * power.GetRotationAmount();

            rotationDuration = power.GetRotationDuration();

            currentRotationTime = 0.0f;
        }

    }

    public static LevelCreator GetLevelCreatorInstance()
    {
        return levelCreator;
    }

    //spawn a new platform
    protected void SpawnPlatform()
    {
        //calculate spawn positions
        spawnPosition.x = previousTile.gameObject.transform.position.x + currentGap + previousTile.GetLength();
        spawnPosition.y = previousTile.transform.position.y;
        spawnPosition.z = character.transform.position.z;

        previousTile = pool.SpawnRandomFromPool(spawnPosition, currentHazardTable, currentCollectableTable);

        currentQueue.Enqueue(previousTile);

        previousTile.transform.SetParent(currentparent.transform);

        if (currentState == LevelState.Bonus)
        {
            currentBonusPlatformCount--;

            if (currentBonusPlatformCount <= 0)
            {
                ChangeState(LevelState.Normal);
            }
        }
    }

    //reset location of parent of the platforms to prevent floating point errors from building up
    protected void ResetPlatformParent()
    {
        GameObject temp = currentparent;

        currentparent = previousparent;
        previousparent = temp;

        foreach (Platform child in currentQueue)
        {
            child.transform.SetParent(currentparent.transform, true);
        }

        previousparent.transform.position = Vector3.zero;
    }

    //increase the game's level and start spawning bonus levels
    protected void LevelUp()
    {
        if(level < levels.Count - 1)
        {
            level++;

            SetLevelVariables();

            levelUpPlatformNumber += platformsUntilLevelUp + bonusLevelLength;

            manager.UpdateScoreModifier();

            SpawnChanceTable.SetLevel(this);

            onLevelUp.Invoke();

            ChangeState(LevelState.Bonus);

            currentBonusPlatformCount = bonusLevelLength;
        }

    }

    //set variables for level change, seperate so it can be called from start
    protected void SetLevelVariables()
    {
        Level currentLevel = null;

        if(level > -1 && level < levels.Count)
        {
            currentLevel = levels[level];

            platformMaxSpeed = currentLevel.maxSpeed;

            currentAcceleration = currentLevel.acceleration;

            platformsUntilLevelUp = currentLevel.normalPlatforms;

            bonusLevelLength = currentLevel.bonusPlatforms;

            currentScoreMultiplier = currentLevel.scoreModifier;

        }

    }


    //change the spawning state of level creator
    protected void ChangeState(LevelState newState)
    {
        switch(newState)
        {
            case LevelState.Bonus:
                {
                    currentGap = bonusLevelGapWidth;
                    currentCollectableTable = bonusTable;
                    currentHazardTable = null;

                    break;
                }

            case LevelState.HighHazard:
                {
                    currentGap = gapWidth;
                    currentCollectableTable = null;
                    currentHazardTable = highHazards;
                    break;
                }

            case LevelState.Normal:
                {
                    currentGap = gapWidth;
                    currentHazardTable = hazardTable;
                    currentCollectableTable = collectableTable;
                    break;
                }
        }

        currentState = newState;

    }

    public int GetLevel()
    {
        return level;
    }

    public float GetScoreMultiplier()
    {
        return currentScoreMultiplier;
    }

}
