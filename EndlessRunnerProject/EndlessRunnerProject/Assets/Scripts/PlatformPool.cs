﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpawnableType { Coin, Obstacle, Heart, FloatingObstacle, RotateWorldPower, InvincibilityPower, RareCoin}

public class PlatformPool : MonoBehaviour
{
    //Pool Class, for platforms
    [Serializable]
    public class Pool
    {
        //Prefab that the pooled objects use
        public GameObject Prefab = null;
        //Size of the pool
        public int Size = 0;

        //likelihood for random spawn
        public float percentage = 1.0f;

    }

    //Pool Class, for objects to spawn onto platforms
    [Serializable]
    public class SpawnablePool
    {
        //Prefab that the pooled objects use
        public GameObject Prefab = null;
        //Size of the pool
        public int Size = 0;

        public SpawnableType type = SpawnableType.Coin;
    }


    [Tooltip("Platform Pool")]
    //Contents of the object pool
    [SerializeField]
    protected List<Pool> pools = new List<Pool>();

    [Tooltip("Spawnable Pool")]
    [SerializeField]
    protected List<SpawnablePool> spawnables = new List<SpawnablePool>();

    //singleton reference
    protected static PlatformPool ObjectPoolInstance = null;

    //Dictionaries
    protected static Dictionary<int, Queue<Platform>> PoolDictionary;
    protected static Dictionary<SpawnableType, Queue<PlatformSpawnable>> SpawnDictionary;

    protected float totalPercentage = 0.0f;
    protected bool platformPoolInitialised = false;
    protected bool SpawnablePoolInitialised = false;

    private void Awake()
    {
        if(!ObjectPoolInstance)
        {
            ObjectPoolInstance = this;
        }
        else
        {
            Debug.LogError("Only one Object Pool should exist!");
            Destroy(this);
        }
    }

    //allows any object in the scene to get a reference to the object pool
    public static PlatformPool GetObjectPoolInstance()
    {
        return ObjectPoolInstance;
    }

    public void CreatePools()
    {
        if(!platformPoolInitialised)
        {
            CreatePlatformPool();
        }
        else
        {
            Debug.Log("CreatePools called when Platform pools already exist!");
        }

        if(!SpawnablePoolInitialised)
        {
            CreateSpawnablePool();
        }
        else
        {
            Debug.Log("CreatePools called when spawnable pools already exist!");
        }

    }

    //creates the platforms for the pool
    protected void CreatePlatformPool()
    {
        if(!platformPoolInitialised)
        {
            platformPoolInitialised = true;

            //Create the dictionary - this allows us to have as many pools as we need and retrieve them via a tag
            PoolDictionary = new Dictionary<int, Queue<Platform>>();

            GameObject currentSpawn = null;
            Platform currentPlatform = null;

            //For each of the pools initialise all of the contents
            for (int i = 0; i < pools.Count; i++)
            {
                Queue<Platform> objectPool = new Queue<Platform>();

                totalPercentage += pools[i].percentage;

                for (int j = 0; j < pools[i].Size; j++)
                {
                    //create a spawn the object and get references to it
                    currentSpawn = Instantiate(pools[i].Prefab, transform);

                    currentPlatform = currentSpawn.GetComponent<Platform>();
                    currentPlatform.SetType(i);

                    if (currentPlatform)
                    {
                        objectPool.Enqueue(currentPlatform);
                    }

                    //Turn off all of the contents, so we only turn them on when they are needed
                    currentSpawn.SetActive(false);

                }

                PoolDictionary.Add(i, objectPool);
            }
        }

    }


    //create the pools for spawnable objects
    protected void CreateSpawnablePool()
    {
        if(!SpawnablePoolInitialised)
        {
            SpawnablePoolInitialised = true;

            SpawnDictionary = new Dictionary<SpawnableType, Queue<PlatformSpawnable>>();

            GameObject currentSpawn = null;

            PlatformSpawnable spawnable = null;

            for(int i=0; i< spawnables.Count; i++)
            {
                Queue<PlatformSpawnable> queue = new Queue<PlatformSpawnable>();

                for(int j=0; j < spawnables[i].Size; j++)
                {
                    currentSpawn = Instantiate(spawnables[i].Prefab, transform);
                    spawnable = currentSpawn.GetComponent<PlatformSpawnable>();

                    //only add valid spawnables to the object pool
                    if(spawnable)
                    {
                        queue.Enqueue(spawnable);
                        spawnable.SetPoolTag(spawnables[i].type);
                    }

                    currentSpawn.SetActive(false);

                }

                //only add to the dictionary if the key doesn't exist and the queue is not empty
                if(!SpawnDictionary.ContainsKey(spawnables[i].type) && queue.Count > 0)
                {
                    SpawnDictionary.Add(spawnables[i].type, queue);
                }

            }
        }

    }

    //Spawns a GameObject from the pool with the given tag at the given position
    protected Platform SpawnFromPool(int tag, Vector3 pos, SpawnChanceTable hazardTable, SpawnChanceTable collectableTable)
    {
        Platform toSpawn = null;

        //Check that the tag exists in the pool
        if (PoolDictionary.ContainsKey(tag))
        {
            //Don't do anything if the pool is empty
            if (PoolDictionary[tag].Count > 0)
            {
                toSpawn = PoolDictionary[tag].Dequeue();

                if(toSpawn)
                {
                    toSpawn.transform.parent = null;

                    toSpawn.transform.position = pos;

                    toSpawn.gameObject.SetActive(true);

                    toSpawn.Spawn(hazardTable, collectableTable);
                }
                else
                {
                    Debug.Log("No object to spawn!");
                }

            }
        }
        else
        {
            Debug.LogWarning("Pool doesn't exist, tag: " + tag);
        }

        return toSpawn;
    }

    //put an object back into the object pool - called by the object
    public void ReturnToPool(Platform enque, int pooltag)
    {
        if(enque)
        {
            //ensure that the dictonary has the correct key
            if (PoolDictionary.ContainsKey(pooltag))
            {
                PoolDictionary[pooltag].Enqueue(enque);
                enque.transform.parent = transform;
                enque.gameObject.SetActive(false);
            }
            else
            {
                Debug.Log(pooltag + " tag is not present in platform pool!");
            }
        }
    }

    //roulette selection of platform
    public Platform SpawnRandomFromPool(Vector3 pos, SpawnChanceTable hazardTable, SpawnChanceTable collectableTable)
    {
        Platform result = null;

        float r = UnityEngine.Random.Range(0.0f, 1.0f);

        float currentPer = 0.0f;

        int chosenPlatform = 0;

        for(int i=0; i<pools.Count; i++)
        {
            currentPer += pools[i].percentage;

            if(currentPer >= r)
            {
                chosenPlatform = i;
                break;
            }
        }

        result = SpawnFromPool(chosenPlatform, pos, hazardTable, collectableTable);

        return result;
    }


    //returns a spawnable object to the pool
    public void ReturnSpawnToPool(PlatformSpawnable spawn, SpawnableType type)
    {
        if(spawn)
        {
            if(SpawnDictionary.ContainsKey(type))
            {
                SpawnDictionary[type].Enqueue(spawn);
                spawn.gameObject.SetActive(false);
                spawn.transform.SetParent(transform);
            }
        }
    }

    //get spawnabale object by it's type from the pool
    public PlatformSpawnable GetSpawn(SpawnableType type, Transform parent)
    {
        PlatformSpawnable result = null;

        if (SpawnDictionary.ContainsKey(type))
        {
            if(SpawnDictionary[type].Count > 0)
            {
                result = SpawnDictionary[type].Dequeue();

                if(result)
                {
                    result.gameObject.SetActive(true);
                    result.transform.SetParent(parent);
                    result.transform.localPosition = Vector3.zero;
                    result.SpawnObject();
                }

            }
            else
            {
                Debug.Log(type + " Pool is currently Empty!");
            }

        }

        return result;
    }
}

