﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Collectable : PlatformSpawnable
{
    [SerializeField]
    protected GameObject model = null;

    protected bool collected = false;

    public UnityEvent onCollect = null;

    protected override void Spawn()
    {
        if(model)
        {
            model.gameObject.SetActive(true);
        }

        collected = false;
    }


    protected void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !collected)
        {
            collected = true;

            if (model)
            {
                model.gameObject.SetActive(false);
            }

            Collect();

            onCollect.Invoke();
        }
    }

    protected virtual void Collect()
    {

    }

}
