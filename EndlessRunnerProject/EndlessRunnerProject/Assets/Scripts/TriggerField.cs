﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider))]
public class TriggerField : MonoBehaviour
{
    [SerializeField]
    protected UnityEvent onExit = null;

    [SerializeField]
    protected UnityEvent onEnter = null;

    [SerializeField]
    protected string TargetedTag = "Player";

    protected BoxCollider boxCollider = null;

    // Start is called before the first frame update
    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();

        boxCollider.isTrigger = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == TargetedTag)
        {
            onExit.Invoke();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == TargetedTag)
        {
            onEnter.Invoke();
        }
    }
}
