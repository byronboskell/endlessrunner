﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePower : Collectable
{
    [SerializeField]
    protected float rotationAmount = 90.0f;

    [SerializeField]
    protected float rotationDuration = 1.0f;

    [SerializeField]
    protected Vector3 rotationDirection = new Vector3(0, 0, 1);

    //store the quaternion rotation to simplify rotation
    protected Quaternion rotationAmountQuan = Quaternion.identity;

    protected override void Setup()
    {
        base.Setup();

        rotationAmountQuan = Quaternion.Euler(rotationAmount * rotationDirection);
    }

    protected override void Collect()
    {
        base.Collect();

        levelCreator.RotateLevel(this);
    }

    public Quaternion GetRotationAmount()
    {
        return rotationAmountQuan;
    }

    public float GetRotationDuration()
    {
        return rotationDuration;
    }
}
