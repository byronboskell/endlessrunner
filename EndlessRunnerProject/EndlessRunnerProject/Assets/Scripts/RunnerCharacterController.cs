﻿using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CharacterController))]
public class RunnerCharacterController : MonoBehaviour
{
    [Header("Jump")]
    [SerializeField]
    protected float jumpCooldown = 1.0f;
    [SerializeField]
    protected float jumpDuration = 0.1f;
    [SerializeField]
    protected float jumpHeight = 5.2f;
    [SerializeField]
    protected float mass = 1.0f;

    [Header("Health")]
    [SerializeField]
    protected int maxHealth = 3;
    [SerializeField]
    protected float invincibilityonDamageTime = 1.0f;
    [SerializeField]
    protected float invincibilityonHealTime = 0.5f;

    [Header("Tinting")]
    [SerializeField]
    protected Color damageTint = Color.red;
    [SerializeField]
    protected Color healTint = Color.green;
    [SerializeField]
    protected Color invincibilityPowerUpColor = Color.yellow;
    [SerializeField]
    protected Renderer characterRenderer = null;
    [SerializeField]
    protected string damageColorName = "_BaseColor";

    [Header("Crouch")]
    [SerializeField]
    protected float crouchHeight = 2f;
    [SerializeField]
    protected Vector3 crouchCentre = Vector3.one;
    [SerializeField]
    protected float crouchTime = 1.0f;
    [SerializeField]
    protected float crouchCooldown = 0.5f;

    [Header("Bounds")]
    [SerializeField]
    protected float deathDistance = 20.0f;

    [SerializeField]
    protected UnityEvent onDeath = null;

    protected CharacterController charController = null;
    protected Animator charAnimator = null;

    protected Vector3 basePosition = Vector3.zero;
    
    protected float normalHeight = 1.7f;
    protected Vector3 normalCentre = Vector3.one;
    protected CapsuleCollider capsule = null;

    protected float currentCrouchTime = 0.0f;
    protected float currentCrouchCooldown = 0.0f;

    protected int currentHealth = 0;

    //Jump speed - calculated based on how high we want the character to jump
    protected float jumpSpeed = 0.0f;

    //Whether or not the character is crouching
    protected bool crouching = false;

    protected float jumpTime = 0.0f;
    protected float jumpCurrentCooldownTime = 0.0f;

    protected float currentInvincibilityTime = 0.0f;

    //Current velocity
    protected Vector3 velocity = Vector3.zero;

    protected bool alive = true;
    protected bool invincible = false;

    protected ScoreManager manager = null;

    //base colour to revert when tinting
    protected Color baseColour = Color.white;

    //singelton instance
    protected static RunnerCharacterController runnerCharacterInstance = null;



    private void Awake()
    {
        if (!runnerCharacterInstance)
        {
            runnerCharacterInstance = this;
        }
        else
        {
            Debug.Log("Character Already Exists!");
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    protected void Start()
    {
        //get referecnes
        charAnimator = GetComponent<Animator>();

        charController = GetComponent<CharacterController>();

        currentCrouchCooldown = crouchCooldown;

        jumpCurrentCooldownTime = jumpCooldown;

        basePosition = gameObject.transform.position;

        if (!characterRenderer)
        {
            characterRenderer = GetComponentInChildren<Renderer>();
        }

        manager = ScoreManager.GetScoreManagerInstance();

        onDeath.AddListener(manager.EndGame);

        if(jumpDuration < 0.01)
        {
            jumpDuration = 0.01f;
        }

        //we ignore mass when moving upwards
        jumpHeight /= mass;

        //calculate jump speed need to reach jump height specified in inspector
        jumpSpeed = Mathf.Sqrt(-2.0f * Physics.gravity.y * jumpHeight);

        //take into account landing back on the ground, so the jump actually takes the correct amount of time
        jumpSpeed /= (jumpDuration * 2);

        //make sure you jumpTime is not below duration (causing jump)
        jumpTime = jumpDuration * 2;

        //set the current health to max
        currentHealth = maxHealth;

        capsule = GetComponent<CapsuleCollider>();

        //record the height of the character's controller
        if(charController)
        {
            normalHeight = charController.height;
            normalCentre = charController.center;

            capsule.height = normalHeight;
            capsule.center = normalCentre;
        }

        //store the base colour of the character for colour changing
        if (characterRenderer)
        {
            baseColour = characterRenderer.material.GetColor(damageColorName);
        }

        if (charAnimator)
        {
            charAnimator.SetBool("OnGround", true);
        }

        if (charAnimator)
        {
            //update the animator values
            charAnimator.SetFloat("Forward", 80);
        }
    }


    // Update is called once per frame
    protected void FixedUpdate()
    {
        if (alive)
        {
            //if the character has recently taken damage reduce vertical
            if (invincible)
            {
                currentInvincibilityTime -= Time.deltaTime;

                if(currentInvincibilityTime < 0)
                {
                    invincible = false;

                    if (characterRenderer)
                    {
                        characterRenderer.material.SetColor(damageColorName, baseColour);
                    }
                }
            }

            if(!charController.isGrounded)
            {
                //apply gravity
                velocity += Physics.gravity * mass * Time.deltaTime;
            }

            jumpTime += Time.deltaTime;

            currentCrouchCooldown += Time.deltaTime;

            if (crouching)
            {
                currentCrouchTime += Time.deltaTime;

                if(currentCrouchTime > crouchTime)
                {
                    Uncrouch();
                }
            }

            //if we are not crouching and a jump is queded add jump velocity
            if (jumpTime < jumpDuration)
            {
                velocity.y = jumpSpeed;
            }
            else
            {
                jumpCurrentCooldownTime += Time.deltaTime;
            }

            //move the character
            charController.Move(velocity * Time.deltaTime);

            CheckBounds();

        }
    }

    //get singleton instance
    public static RunnerCharacterController GetRunnerInstance()
    {
        return runnerCharacterInstance;
    }

    //take some damage
    public void TakeDamage(PlatformSpawnable spawnable)
    {
        if (spawnable)
        {

            int newHealth = spawnable.ChangeHealth(currentHealth, out float newInvincibilityTime);

            Color invinColor = invincibilityPowerUpColor;

            if (newHealth < currentHealth)
            {
                if(invincible)
                {
                    newHealth = currentHealth;
                    newInvincibilityTime = 0.0f;
                }

                invinColor = damageTint;

            }
            else if(newHealth > currentHealth)
            {
                invinColor = healTint;

                if(newHealth > maxHealth)
                {
                    newHealth = maxHealth;
                }
            }


            currentHealth = newHealth;

            if (manager)
            {
                manager.UpdateHealth(currentHealth);
            }

            if (currentHealth <= 0)
            {
                Death();
            }
            else
            {
                BecomeInvincible(newInvincibilityTime, invinColor);
            }
        }
    }

    //handles invincibility
    protected void BecomeInvincible(float time, Color tint)
    {
        if (alive && currentInvincibilityTime < time && time > 0)
        {
            currentInvincibilityTime = time;

            invincible = true;

            //tint the character's colour
            if (characterRenderer)
            {
                characterRenderer.material.SetColor(damageColorName, tint);
            }

        }
    }

    //Makes the character Jump
    public void Jump()
    {
        if(crouching)
        {
            Uncrouch();
        }

        if (jumpCurrentCooldownTime > jumpCooldown && charController.isGrounded)
        {
            jumpCurrentCooldownTime = jumpCooldown;
            jumpTime = 0;
        }
    }

    //make the character crouch (to avoid obstacles above them)
    public void Crouch()
    {
        if(currentCrouchCooldown > crouchCooldown  && !crouching)
        {
            crouching = true;

            if (charAnimator)
            {
                charAnimator.SetBool("Crouch", crouching);
            }

            charController.height = crouchHeight;
            charController.center = crouchCentre;

            //change size of object colliding with character collider
            capsule.height = crouchHeight;
            capsule.center = crouchCentre;

            currentCrouchTime = 0.0f;
        }
    }

    //stops crouching
    public void Uncrouch()
    {
        if(crouching)
        {
            crouching = false;

            if (charAnimator)
            {
                charAnimator.SetBool("Crouch", crouching);
            }

            charController.height = normalHeight;
            charController.center = normalCentre;

            //change size of object colliding with character collider
            capsule.height = normalHeight;
            capsule.center = normalCentre;

            currentCrouchCooldown = 0.0f;
        }

    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    //function called when a character falls too far or else takes too much damage - ends the game
    protected void Death()
    {
        onDeath.Invoke();

        alive = false;

        gameObject.SetActive(false);
    }

    //check if the player is too far from the centre
    protected void CheckBounds()
    {
        float distance = Vector3.Distance(gameObject.transform.position, basePosition);

        if(distance > deathDistance)
        {
            Death();
        }
    }


    //coroutine to change colour tint for a short amount of time
    protected void ChangeColor(Color TempColor)
    {
        if(characterRenderer)
        {
            characterRenderer.material.SetColor(damageColorName, TempColor);

        }

    }
}
