﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : Collectable
{
    [SerializeField]
    protected int healAmount = 1;

    [SerializeField]
    protected float invincibilityOnHeal = 0.5f;

    protected override void Collect()
    {
        base.Collect();

        if (controller)
        {
            controller.TakeDamage(this);
        }
    }

    //Secure function that changes health
    public override int ChangeHealth(int health, out float invincibilityTime)
    {
        health += Mathf.Abs(healAmount);

        invincibilityTime = invincibilityOnHeal;

        return health;

    }
}
