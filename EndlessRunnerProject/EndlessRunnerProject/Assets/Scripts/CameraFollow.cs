﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject followTarget = null;

    public float zOffset = 1.0f;

    protected Vector3 position = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        position = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
         if(followTarget)
         {
            position.x = followTarget.transform.position.x;
            position.z = followTarget.transform.position.z + zOffset;

            transform.position = position;
         }
    }
}
