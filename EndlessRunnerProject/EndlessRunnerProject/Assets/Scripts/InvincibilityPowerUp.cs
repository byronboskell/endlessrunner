﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibilityPowerUp : Collectable
{
    [SerializeField]
    protected float powerUpInvincibility = 2.0f;

    protected override void Collect()
    {
        base.Collect();

        if(controller)
        {
            controller.TakeDamage(this);
        }
    }

    //secure way to send invincibility time
    public override int ChangeHealth(int health, out float invincibilityTime)
    {
        invincibilityTime = powerUpInvincibility;

        return health;
    }
}
