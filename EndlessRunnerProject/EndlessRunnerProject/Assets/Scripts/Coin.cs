﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Collectable
{
    [SerializeField]
    protected int ScoreAmount = 100;

    protected static ScoreManager scoreManager = null;

    protected override void Setup()
    {
        base.Setup();

        if (!scoreManager)
        {
            scoreManager = ScoreManager.GetScoreManagerInstance();
        }
    }

    protected override void Collect()
    {
        base.Collect();

        if(scoreManager)
        {
            scoreManager.AddScore(this);
        }
    }

    //secure way to change score
    public override int ChangeScore(int score)
    {
        return score + ScoreAmount;
    }
}
