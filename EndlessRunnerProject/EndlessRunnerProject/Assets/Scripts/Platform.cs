﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Platform : MonoBehaviour
{
    [Header("Slots")]
    [SerializeField]
    protected List<GameObject> collectableSlots = new List<GameObject>();

    [SerializeField]
    protected List<GameObject> hazardSlots = new List<GameObject>();


    protected BoxCollider box = null;

    protected static PlatformPool pool = null;

    protected bool typeSet = false;
    protected int type = 0;

    //stores all of the spawnable objects into a stack for easy addition and removal
    protected Stack<PlatformSpawnable> spawnableStack = new Stack<PlatformSpawnable>();

    // Start is called before the first frame update
    void Start()
    {
        box = GetComponent<BoxCollider>();

        gameObject.tag = "Floor";

        if (!pool)
        {
            pool = PlatformPool.GetObjectPoolInstance();
        }
    }

    public float GetLength()
    {
        float result = 0.0f;

        if (box)
        {
            result = box.size.x;
        }

        return result;
    }

    //returns the platforms and any objects it spawned to the pool
    public void ReturnToPool()
    {
        //if the type of the platform is not set we don't know which part of the object pool to send it to
        if (typeSet && pool)
        {
            PlatformSpawnable currentSpawnable = null;

            //return all the objects in the stack to the object pool
            while (spawnableStack.Count > 0)
            {
                currentSpawnable = spawnableStack.Pop();

                if (currentSpawnable)
                {
                    currentSpawnable.ReturnToPool();
                }
            }

            //return the object to the object pool
            pool.ReturnToPool(this, type);
        }
        else
        {
            Debug.Log("Type not set! Cannot return to pool");
            gameObject.SetActive(false);
        }

    }


    //sets the objectpooling type can only be done once
    public void SetType(int newType)
    {
        if (!typeSet)
        {
            type = newType;

            typeSet = true;
        }
    }

    //function called on spawn to setup the platform
    public void Spawn(SpawnChanceTable hazardTable, SpawnChanceTable collectableTable)
    {
        //spawn all collectables
        SpawnSpawnableType(collectableSlots, collectableTable);

        //spawn all hazards
        SpawnSpawnableType(hazardSlots, hazardTable);
    }

    //spawns collectables based on tables and slots
    protected void SpawnSpawnableType(List<GameObject> slots, SpawnChanceTable table)
    {
        int max = 0;

        if(table)
        {
            max = table.GetMaxSpawnable(slots.Count);

            if (slots.Count > 0 && max >= 1)
            {
                PlatformSpawnable spawnable = null;

                int startIndex = 0;

                int endIndex = slots.Count - 1;

                int currentIndex = -1;

                if (slots.Count > 0 && table)
                {
                    for (int i = 0; i < max; i++)
                    {
                        //roll to see if there is a spawn
                        if (table.RollSpawnChance())
                        {
                            //choose the index for the spawn
                            currentIndex = ChooseIndex(startIndex, endIndex);

                            //if index is valid spawn the object in the slot
                            if (currentIndex > -1 && currentIndex < slots.Count)
                            {
                                spawnable = table.SpawnObject(slots[currentIndex].transform);

                                if (spawnable)
                                {
                                    spawnableStack.Push(spawnable);

                                    //swap the slots around so the unused ones are contigous
                                    GameObject temp = slots[startIndex];

                                    slots[startIndex] = slots[currentIndex];

                                    slots[currentIndex] = temp;

                                    startIndex++;

                                    //if the start index is greater than the end index there are no more valid positions left
                                    if (startIndex >= endIndex)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    //choose index to spawn something in
    protected int ChooseIndex(int startRange, int endRange)
    {
        int chosenIndex = -1;

        if (startRange > endRange)
        {
            int temp = startRange;

            startRange = endRange;
            endRange = temp;
        }

        chosenIndex = UnityEngine.Random.Range(startRange, endRange);
        
        return chosenIndex;
    }

#if UNITY_EDITOR

    //visualview of slots
    protected void OnDrawGizmos()
    {
        if (collectableSlots.Count > 0)
        {
            Gizmos.color = Color.yellow;
            for (int i = 0; i < collectableSlots.Count; i++)
            {
                Gizmos.DrawSphere(collectableSlots[i].transform.position, 1);
            }
        }

        if(hazardSlots.Count > 0)
        {
            Gizmos.color = Color.red;
            for (int i=0; i<hazardSlots.Count; i++)
            {
                Gizmos.DrawSphere(hazardSlots[i].transform.position, 1);
            }
        }
    }
#endif

}
