﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.IO;
using System;
using UnityEngine.UI;
using System.Text;
using UnityEngine.SceneManagement;

[Serializable]
public class ScoreNamePair
{
    public int value = 0;
    public string name = "NAN";
}

[Serializable]
public class HighScoreSave
{
    public List<ScoreNamePair> highScore = new List<ScoreNamePair>();

    protected static int highScoreTableSize = 10;

    public void ResetTable()
    {
        for (int i = 0; i < highScoreTableSize; i++)
        {
            highScore.Add(new ScoreNamePair());
        }
    }
}

public class ScoreManager : MonoBehaviour
{
    [Header("Highscore")]
    [SerializeField]
    protected GameObject highscoreTable = null;
    [SerializeField]
    protected Text scoreText = null;
    [SerializeField]
    protected InputName inputName = null;
    [SerializeField]
    protected List<Text> highScoreTexts;
    [SerializeField]
    protected GameObject Menu = null;

    [Header("Message")]
    [SerializeField]
    protected Text MessageText = null;
    [SerializeField]
    protected GameObject MessageParent = null;
    [SerializeField]
    protected float MessageDuration = 1.0f;

    [Header("Health")]
    [SerializeField]
    protected GameObject healthUIPrefab = null;
    [SerializeField]
    protected GameObject healthPrefabParent = null;
    [SerializeField]
    protected Color heartFilledColour = Color.red;
    [SerializeField]
    protected Color heartEmptyColour = Color.white;

    [Header("Score")]
    protected float timeScoreMultiplier = 8.0f;
    protected int pickupScore = 0;
    protected float timeScore = 0.0f;

    //how big is the highscoretable?
    protected int highScoreListSize = 10;

    protected float currentMessageTime = 0.0f;
    protected bool messageShown = false;

    protected bool gameOver = false;

    protected RunnerCharacterController character = null;

    protected LevelCreator levelCreator = null;

    //singleton scoremanager
    protected static ScoreManager scoreManagerInstance = null;

    protected static HighScoreSave save = null;

    protected List<Image> heartImages = new List<Image>();

    protected string currentName = "NAN";

    protected readonly string playerPrefSaveName = "highscore";

    // Start is called before the first frame update
    void Awake()
    {
        //singleton pattern
        if (!scoreManagerInstance)
        {
            scoreManagerInstance = this;
        }
        else
        {
            Destroy(this);
            Debug.Log("An instance of scoremanager already exists");
        }

    }

    protected void Start()
    {
        //the save file is static so can be used across scenes
        //if it hasn't been loaded load the save file
        if (save == null)
        {
            LoadHighScores();
        }

        //get an reference to the character controller
        character = RunnerCharacterController.GetRunnerInstance();

        levelCreator = LevelCreator.GetLevelCreatorInstance();

        //set up health icons
        if (character && healthUIPrefab && healthPrefabParent)
        {
            int hearts = character.GetMaxHealth();

            GameObject currentObject = null;

            Image currentImage = null;

            for (int i = 0; i < hearts; i++)
            {
                currentObject = Instantiate(healthUIPrefab, healthPrefabParent.transform);

                currentImage = currentObject.GetComponent<Image>();

                if (currentImage)
                {
                    heartImages.Add(currentImage);
                }
            }

            UpdateHealth(hearts);

        }

        //Turn off UI elements not currently needed by the game
        if (inputName)
        {
            inputName.gameObject.SetActive(false);
        }

        DisplayHighScores();

        ShowHighScore(false);

        ToggleMenu(false);
    }

    //get the singleton instance
    public static ScoreManager GetScoreManagerInstance()
    {
        return scoreManagerInstance;
    }

    // Update is called once per frame
    void Update()
    {
        if(!gameOver)
        {
            timeScore += Time.deltaTime * timeScoreMultiplier;

            if (scoreText)
            {
                scoreText.text = "Score: " + (pickupScore + (int)(timeScore));
            }

            if(messageShown && MessageParent)
            {
                currentMessageTime += Time.deltaTime;

                if(currentMessageTime > MessageDuration)
                {
                    MessageParent.SetActive(false);
                    messageShown = false;
                }
            }

        }
    }

    //adds extra score
    public void AddScore(PlatformSpawnable coin)
    {
        if(coin)
        {
            pickupScore = coin.ChangeScore(pickupScore);
        }
    }

    //load highscore from a save file (stored in player prefs)
    protected void LoadHighScores()
    {
        string savefile = "";

        if (PlayerPrefs.HasKey(playerPrefSaveName))
        {
            savefile = PlayerPrefs.GetString(playerPrefSaveName);

            //if savefile isn't empty try to deserialise
            if (savefile.Length > 0)
            {
                TextReader reader = new StringReader(savefile);


                XmlSerializer formatter = new XmlSerializer(typeof(HighScoreSave));

                try
                {
                    save = (HighScoreSave)formatter.Deserialize(reader);

                    if(save.highScore.Count == 0)
                    {
                        ResetHighScoreTable();
                    }
                }
                catch (System.Runtime.Serialization.SerializationException)
                {
                    ResetHighScoreTable();
                    Debug.Log("Cannot Read SaveFile");
                }
                finally
                {

                }

                reader.Close();
            }

        }
        else
        {
            ResetHighScoreTable();
        }
    }

    //save the highscores
    protected void SaveHighScores()
    {
        StringWriter stream = new StringWriter();

        XmlSerializer formatter = new XmlSerializer(typeof(HighScoreSave));

        formatter.Serialize(stream, save);

        string file = stream.ToString();

        PlayerPrefs.SetString(playerPrefSaveName, file);
        stream.Close();
    }

    //check the highscores
    public void CheckHighScore()
    {
        int finalScore = pickupScore + (int)timeScore;

        for (int i = 0; i < save.highScore.Count; i++)
        {
            if (save.highScore[i].value < finalScore)
            {
                ChangeScore(finalScore, currentName);
                break;
            }
        }

        DisplayHighScores();
        ToggleMenu(true);
    }

    //If there is a new highscore add it to the highscore table
    protected void ChangeScore(int newScore, string newName)
    {
        save.highScore[save.highScore.Count - 1].value = newScore;
        save.highScore[save.highScore.Count - 1].name = newName;
        save.highScore.Sort((a, b) => b.value.CompareTo(a.value));
        SaveHighScores();
    }

    //change the name as a string
    public void ChangeName(string newName)
    {
        currentName = newName;

        CheckHighScore();

        if (inputName)
        {
            inputName.gameObject.SetActive(false);
        }

    }

    //show the highscores
    public void DisplayHighScores()
    {
        for (int i = 0; i < highScoreTexts.Count; i++)
        {
            if (i < save.highScore.Count)
            {
                highScoreTexts[i].text = save.highScore[i].name + "             " + save.highScore[i].value;
            }
            else
            {
                highScoreTexts[i].text = "Cannot Read Highscore";
            }
        }
    }


    //show the input name canvas
    public void ShowInputName()
    {
        if (inputName)
        {
            inputName.gameObject.SetActive(true);
        }
    }

    //resets the highscore table
    public void ResetHighScoreTable()
    {
        save = new HighScoreSave();

        save.ResetTable();

        SaveHighScores();
    }

    //show the pause menu
    public void ToggleMenu(bool status)
    {
        if (Menu)
        {
            Menu.SetActive(status);
        }

        //manage the timescale to pause the game
        if(!gameOver)
        {
            if (status)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
        else
        {
            //if the game is over time should not pass
            Time.timeScale = 0;
        }


    }

    //update the level based on level creator's level
    public void UpdateScoreModifier()
    {
        if(levelCreator)
        {
            int newLevel = levelCreator.GetLevel();

            timeScoreMultiplier = levelCreator.GetScoreMultiplier();
        }
    }

    //show the highscores panel
    public void ShowHighScore(bool status)
    {
        if(highscoreTable)
        {
            highscoreTable.SetActive(status);
        }

    }

    //updates health UI
    public void UpdateHealth(int value)
    {
        for(int i=0; i<heartImages.Count; i++)
        {
            if(i < value)
            {
                heartImages[i].color = heartFilledColour;
            }
            else
            {
                heartImages[i].color = heartEmptyColour;
            }
        }
    }

    //Stop the game and open the name inputting dialogue
    public void EndGame()
    {
        gameOver = true;
        Time.timeScale = 0;

        ShowInputName();
    }

    public void ShowMessage(string message)
    {
        if(MessageText && MessageParent)
        {
            MessageParent.SetActive(true);
            MessageText.text = message;
            currentMessageTime = 0.0f;
            messageShown = true;
        }
    }

    //go to scene with the specified name
    public void GoToScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    //restart the current level by reloading it
    public void RestartLevel()
    {
        string sceneName = SceneManager.GetActiveScene().name;

        SceneManager.LoadScene(sceneName);
    }

    //exit the game
    public void ExitGame()
    {
        Application.Quit();
    }


}

